describe('movie search service', function () {
     var movieData = { "Title": "Star Wars", "Year": "1983", "Rated": "N/A", "Released": "01 May 1983", "Runtime": "N/A", "Genre": "Action, Adventure, Sci-Fi", "Director": "N/A", "Writer": "N/A", "Actors": "Harrison Ford, Alec Guinness, Mark Hamill, James Earl Jones", "Plot": "N/A", "Language": "English", "Country": "USA", "Awards": "N/A", "Poster": "http://ia.media-imdb.com/images/M/MV5BMWJhYWQ3ZTEtYTVkOS00ZmNlLWIxZjYtODZjNTlhMjMzNGM2XkEyXkFqcGdeQXVyNzg5OTk2OA@@._V1_SX300.jpg", "Metascore": "N/A", "imdbRating": "7.9", "imdbVotes": "356", "imdbID": "tt0251413", "Type": "game", "Response": "True" };

     var movieById = {"Title":"Mechanic","Year":"2013","Rated":"N/A","Released":"13 Jul 2013","Runtime":"15 min","Genre":"Short, Drama","Director":"Feidlim Cannon, Tom Sullivan","Writer":"Feidlim Cannon, Tom Sullivan, Tom Sullivan","Actors":"Sil Fox, Paul Roe","Plot":"Kev, a mechanic in his 40s, drives up the Dublin mountains to end his life. He is cruelly interrupted by an old man who is lost. Kev is forced to make some hard decisions, for better or worse.","Language":"English","Country":"Ireland","Awards":"1 win & 1 nomination.","Poster":"N/A","Metascore":"N/A","imdbRating":"6.8","imdbVotes":"10","imdbID":"tt3159574","Type":"movie","Response":"True"};

     var omdbApi = {};

     beforeEach(module('omdb'));

     beforeEach(inject(function (_omdbApi_) {
            omdbApi = _omdbApi_;
        }));

        console.log(angular.mock.dump(movieData));
        


    it('should return search result', function () {
      expect(omdbApi.search('star wars')).toEqual(movieData);
    });

    it('should return movie by id',function(){
        expect(omdbApi.find('tt3159574')).toEqual(movieById);
    });

})

    ;